/*
 # Class defenitions
 */

struct Crypto
{
    let name: String
    let price: Double
    let exchange: String
}

struct CoinDict
{
    let name: String
    let exchanges: Array<Crypto>
}


extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}

extension Crypto: Comparable,Equatable
{
    static func < (lhs: Crypto, rhs: Crypto) -> Bool
    {
        return lhs.price < rhs.price
    }
    
    
    static func == (lhs: Crypto, rhs: Crypto) -> Bool
    {
        return lhs.price == rhs.price
    }
    
}

func calculateProfit(_ exchange1:Crypto,_ exchange2: Crypto) -> Float
{
    let difference = Float(exchange2.price - exchange1.price)
    return difference/Float(exchange1.price)*100
}


let bitbnsEth = Crypto(name: "ether", price: 55600, exchange: "bitbns")
let bitbnsXRP = Crypto(name: "xrp", price: 62, exchange: "bitbns")
let bitbnsLTC = Crypto(name: "ltc", price: 12600, exchange: "bitbns")

let bitbnsArray = [bitbnsEth,bitbnsXRP,bitbnsLTC]


let koinEth = Crypto(name: "ether", price: 53600, exchange: "koin")
let koinXRP = Crypto(name: "xrp", price: 63, exchange: "koin")
let koinLTC = Crypto(name: "ltc", price: 12800, exchange: "koin")

let koinArray = [koinEth,koinLTC,koinXRP]


let deltaEth = Crypto(name: "ether", price: 54600, exchange: "delta")
let deltaXRP = Crypto(name: "xrp", price: 61, exchange: "delta")
let deltaLTC = Crypto(name: "ltc", price: 16800, exchange: "delta")

let deltaArray = [deltaEth,deltaLTC,deltaXRP]


let pcktbitsEth = Crypto(name: "ether", price: 54600, exchange: "pcktbits")
let pcktbitsXRP = Crypto(name: "xrp", price: 61, exchange: "pcktbits")
let pcktbitsLTC = Crypto(name: "ltc", price: 16800, exchange: "pcktbits")
let pcktbitsNeo = Crypto(name: "neo", price: 7000, exchange: "pcktbits")

let pcktbitsArray = [pcktbitsEth,pcktbitsLTC,pcktbitsXRP,pcktbitsNeo]





func transformToCoinBased()
{
    let allExchangeArray = [koinArray,bitbnsArray,deltaArray,pcktbitsArray]
    let flattened_all_exchanges = allExchangeArray.flatMap{return $0}
    
    // Gets all coins and finds the unique coins across exchanges
    let allCoins = flattened_all_exchanges.map{$0.name}
    let uniqueCoins = Array(Set(allCoins))
    // Decouples and stores coins based on type of coin rather than type of Exchange
    let arrayOfCoins = uniqueCoins.map{ (coin) -> [Crypto] in
        let coins = flattened_all_exchanges.filter{
            value in
            
            return value.name == coin
        }
        return coins
    }
    
}




// This is how we'll get all the data from server
let allExchangeArray = [koinArray,bitbnsArray,deltaArray,pcktbitsArray]
// Flat map flattens the structure and makes it to a single dimensional array
let flattened_all_exchanges = allExchangeArray.flatMap{return $0}
// Gets all coins and finds the unique coins across exchanges
let allCoins = flattened_all_exchanges.map{$0.name}
let uniqueCoins = Array(Set(allCoins))
// Decouples and stores coins based on type of coin rather than type of Exchange
let arrayOfCoins = uniqueCoins.map{ (coin) -> [Crypto] in
    let coins = flattened_all_exchanges.filter{
        value in
        return value.name == coin
    }
    return coins
}
//Sorts the array of coins which are already filtered based on type of the coin. 
let sortedCoinArrays = arrayOfCoins.map {$0.sorted()}
print("Sorted", sortedCoinArrays)
// -----------------------------------------------------------------------------------------//
for sortedCoin in sortedCoinArrays
{
    let profitPercent = calculateProfit(sortedCoin.first!,sortedCoin.last!)
    print("Max profit for \(sortedCoin.first!.name) is \(profitPercent) -> Trade between \(sortedCoin.first!.exchange) and \(sortedCoin.last!.exchange)")
}

// -----------------------------------------------------------------------------------------//

var dict  = [String: Array<Crypto>] ()
for sortedCoin in sortedCoinArrays
{
    let key = sortedCoin.first!.name
    let value = sortedCoin
    dict[key] = value
}

print("lol \n\n\n\n",dict["ltc"]!)
// -----------------------------------------------------------------------------------------//

let coinBasedArray = sortedCoinArrays.map{ (coinArray) -> CoinDict in
    let coin = coinArray.first!.name
    return CoinDict(name: coin, exchanges: coinArray)
}

print("\n\n\n Coinbased array \n\n\n",coinBasedArray,"\n\n\n\n")

// -----------------------------------------------------------------------------------------////

// Get particular coin
func getParticularCoin(_ coin:String) -> Array<Crypto>
{
    let coinDict =  coinBasedArray.filter{ value in
        return value.name == coin
    }
    return coinDict.first!.exchanges
}
print("\n \n \n Particular coin: ",getParticularCoin("ether"))




// -----------------------------------------------------------------------------------------//

findBestPair(bitbnsArray)
func findBestPair(_ exchange: Array<Crypto>)
{
    let coins_in_exchange = exchange.map{$0.name}
    let rest_of_exchanges = allExchangeArray.filter{$0 != exchange}
    let flatten_exchanges = rest_of_exchanges.flatMap{return $0}
    print(coins_in_exchange)
    
    let arrayOfCoins = coins_in_exchange.map{ (coin) -> [Crypto] in
        let coins = flatten_exchanges.filter {
            value in
            value.name == coin
        }
        
        return coins
    }
    
    let sortedCoins = arrayOfCoins.map {return $0.sorted()}
    print(sortedCoins)
    
    for sortedCoin in sortedCoins
    {
        let coin = sortedCoin.first!.name
        let exchange_crypto = exchange.filter{$0.name == coin}
        let current_coin_in_current_exchange = exchange_crypto.first!
        
        print("Profit for \(coin)", calculateProfit(current_coin_in_current_exchange, sortedCoin.first!),"between exchanges \(current_coin_in_current_exchange.exchange) : \((current_coin_in_current_exchange.price)) and \(sortedCoin.first!.exchange): \(sortedCoin.first!.price)")
    }
    
    
    
//    for coin in flatten_exchanges
//    {
//        let key = coin.first!.name
//        let value = coin
//        exchangeDict[key] = value
//    }
//
//    for coin in coins_in_exchange
//    {
//        let sortedArray = dict[coin]!
//        sortedArray.reversed()
//        let percentageMax = calculateProfit(exchangeDict[coin]!, sortedArray)
//    }
    
    
}
