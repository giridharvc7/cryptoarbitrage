//
//  BinanceAsset.swift
//  CryptoArbitage
//
//  Created by Giridhar on 08/01/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Binance: CryptoAsset
{
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    
    var price: Double = 0.0
    
    var symbol: String = " "
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = ""
    
    var exchangeName: Exchange = .binance
    
    var baseURL: String = "https://api.binance.com/api/v3/ticker/price"

    private func splitSymbol(symbol: String) -> TradingPair
    {
        let formattedSymbol = symbol.lowercased()
        let allPairs = [Coin.eth.rawValue, Coin.btc.rawValue, Coin.ltc.rawValue, Coin.neo.rawValue, Coin.xrp.rawValue, Coin.usdt.rawValue]
        var mainCoin = " "
        let endIndex: String.Index
        if formattedSymbol.contains(Coin.usdt.rawValue)
        {
             endIndex = formattedSymbol.index(formattedSymbol.endIndex, offsetBy: -4)
        }
        else
        {
             endIndex = formattedSymbol.index(formattedSymbol.endIndex, offsetBy: -3)
        }
        let pairCoin = String(formattedSymbol[endIndex...])
        mainCoin = formattedSymbol.replacingOccurrences(of: pairCoin, with: "")
        let trade: TradingPair = (mainCoin, pairCoin)
        return trade
        
    }
    
    
}

extension Binance
{
    init(json: JSON)
    {
        self.price = Double(json["price"].string!)!
        self.symbol = json["symbol"].string!
        self.tradingPair = self.splitSymbol(symbol: symbol)
        self.mainCoin = self.tradingPair.0
        self.pairCoin = self.tradingPair.1
        
        self.buying_price = self.price
        self.selling_price = self.price
    }
    
}
