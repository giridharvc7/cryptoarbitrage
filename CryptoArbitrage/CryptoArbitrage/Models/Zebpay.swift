//
//  Zebpay.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 11/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

struct Zebpay: CryptoAsset {
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0

    var symbol: String = ""

    var tradingPair: TradingPair = ("","")

    var mainCoin: String = ""

    var pairCoin: String = "inr"

    var price: Double = 0.0

    var exchangeName: Exchange = .zebpay

    var baseURL: String = ""
    
    var json: JSON = JSON()

    let urlList = ["https://www.zebapi.com/api/v1/market/ticker-new/btc/inr",
                   "https://www.zebapi.com/api/v1/market/ticker-new/eth/inr",
                   "https://www.zebapi.com/api/v1/market/ticker-new/ltc/inr",
                   "https://www.zebapi.com/api/v1/market/ticker-new/xrp/inr"]



}

extension Zebpay
{
    init(json: JSON)
    {
        self.exchangeName = .zebpay
        self.price = json["buy"].doubleValue
        self.mainCoin = json["virtualCurrency"].stringValue
        self.pairCoin = "inr"
        self.tradingPair = (mainCoin,pairCoin)
        self.symbol = self.mainCoin
        self.buying_price = json["buy"].doubleValue
        self.selling_price = json["sell"].doubleValue

    }
}

