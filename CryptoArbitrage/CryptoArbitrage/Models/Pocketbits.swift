//
//  Pocketbits.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 14/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation

import SwiftyJSON

struct Pocketbits: CryptoAsset {
    
    
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var baseURL: String = "https://pocketbits.in/api/tickerall"
    
    var symbol: String = ""
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = ""
    
    var price: Double = 0
    
    var exchangeName: Exchange = .pocketbits
    
    var json: JSON = JSON()
}
extension Pocketbits
{
    init(json: JSON)
    {
        self.pairCoin = "inr"
        self.mainCoin = json["AltName"].stringValue
        self.price = json["AltSellPrice"].doubleValue
        self.tradingPair = (self.mainCoin,self.pairCoin)
        self.buying_price = json["AltBuyPrice"].doubleValue
        self.selling_price = json["AltSellPrice"].doubleValue
        self.symbol = json["AltName"].stringValue
    }
    
}
