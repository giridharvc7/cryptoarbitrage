//
//  Koinex.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 10/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Koinex: CryptoAsset {
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var baseURL: String = "https://koinex.in/api/ticker"
    
    var symbol: String = ""
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = ""
    
    var price: Double = 0
    
    var exchangeName: Exchange = .koinex
    
    var json: JSON = JSON()
}
extension Koinex
{
    init(json: JSON) {
        self.symbol = json["coin"].stringValue
        self.mainCoin = self.symbol
        self.pairCoin = "inr"
        self.price = json["last_traded_price"].doubleValue
        self.tradingPair = (self.mainCoin,self.pairCoin)
        self.buying_price = json["highest_bid"].doubleValue
        self.selling_price = json["lowest_ask"].doubleValue
        
    }
}

