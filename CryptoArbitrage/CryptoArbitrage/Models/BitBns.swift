//
//  BitBns.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 10/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

struct BitBns: CryptoAsset {
    
    
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var baseURL: String = "https://bitbns.com/order/getTickerAll"
    
    var symbol: String = ""
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = ""
    
    var price: Double = 0
    
    var exchangeName: Exchange = .bitbns
    
    var json: JSON = JSON()
}
extension BitBns
{
    init(json: JSON) {
        let newJson = json.dictionaryValue
        self.symbol = newJson.keys.first!
        self.mainCoin = self.symbol
        self.pairCoin = "inr"
        let priceDict:JSON = newJson[symbol]!
        self.price = priceDict["lastTradePrice"].doubleValue
        self.tradingPair = (self.mainCoin,self.pairCoin)
        self.buying_price = priceDict["buyPrice"].doubleValue
        self.selling_price = priceDict["sellPrice"].doubleValue
        
    }
}
