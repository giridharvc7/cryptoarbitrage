//
//  Kucoin.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 08/01/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Kucoin: CryptoAsset
{
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var price: Double = 0.0
    
    var symbol: String = " "
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = ""
    
    var exchangeName: Exchange = .kucoin
    
    var baseURL: String = "https://api.kucoin.com/v1/open/tick"
    
    var json: JSON = JSON()
}

extension Kucoin
{
    init (json: JSON)
    {
//        self.json = json["data"].arrayValue
        self.symbol = json["symbol"].string!
        self.mainCoin = json["coinType"].string!
        self.pairCoin = json["coinTypePair"].string!
        self.tradingPair = (mainCoin,pairCoin)
        self.price = json["buy"].doubleValue
        self.json = json
        self.buying_price = self.price
        self.selling_price = self.price
    }
}
