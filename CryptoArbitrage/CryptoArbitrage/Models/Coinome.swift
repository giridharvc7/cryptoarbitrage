//
//  Coinome.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 11/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Coinome: CryptoAsset
{
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var symbol: String = ""
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = "inr"
    
    var price: Double = 0.0
    
    var exchangeName: Exchange = .coinome
    
    var baseURL: String = "https://www.coinome.com/api/v1/ticker.json"
    
    var json: JSON = JSON()
    
    private func splitSymbol(symbol: String) -> TradingPair
    {
        let pair = symbol.split(separator: "-")
        return TradingPair(String(describing: pair.first!),String(describing: pair.last!))
    }


}

extension Coinome
{
    init(json: JSON)
    {
        self.json = json
        self.price = json["last"].doubleValue
        self.buying_price = json["highest_bid"].doubleValue
        self.selling_price = json["lowest_ask"].doubleValue
        self.symbol = json["coin"].string!
        self.tradingPair = self.splitSymbol(symbol: symbol)
        self.mainCoin = self.tradingPair.0
        self.pairCoin = self.tradingPair.1
    }
}
