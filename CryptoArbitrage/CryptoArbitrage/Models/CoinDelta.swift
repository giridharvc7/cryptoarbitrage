//
//  CoinDelta.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 17/02/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON


struct CoinDelta: CryptoAsset {
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var symbol: String = ""
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""

    var pairCoin: String = ""
    
    var price: Double = 0
    
    var exchangeName: Exchange = .coindelta
    
    var json: JSON = JSON()
    
    var baseURL: String = "https://coindelta.com/api/v1/public/getticker/"
    
    private func splitSymbol(symbol: String) -> TradingPair
    {
        let pair = symbol.split(separator: "-")
        return TradingPair(String(describing: pair.first!),String(describing: pair.last!))
    }

}

extension CoinDelta
{
 
    init(json: JSON)
    {
        self.json = json
        self.price = json["Ask"].doubleValue
        self.symbol = json["MarketName"].string!
        self.tradingPair = self.splitSymbol(symbol: symbol)
        self.mainCoin = self.tradingPair.0
        self.pairCoin = self.tradingPair.1
        self.buying_price = json["Bid"].doubleValue
        self.selling_price = json["Ask"].doubleValue
    }
    
}
