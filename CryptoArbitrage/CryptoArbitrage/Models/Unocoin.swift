//
//  Unocoin.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 14/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Unocoin: CryptoAsset {
    
    
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var baseURL: String = "https://www.unocoin.com/trade?exchange"
    
    var symbol: String = ""
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = ""
    
    var price: Double = 0
    
    var exchangeName: Exchange = .unocoin
    
    var json: JSON = JSON()
}
extension Unocoin
{
    init(json: JSON)
    {
        self.pairCoin = "inr"
        self.mainCoin = json["coin"].stringValue
        self.price = json["lowest_ask"].doubleValue
        self.tradingPair = (self.mainCoin,self.pairCoin)
        self.buying_price = json["highest_bid"].doubleValue
        self.selling_price = json["lowest_ask"].doubleValue
        self.symbol = json["coin"].stringValue
    }
}
