//
//  BuyUCoin.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 13/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON




struct BuyUCoin: CryptoAsset {
    
    
    var buying_price: Double = 0.0
    
    var selling_price: Double = 0.0
    
    var baseURL: String = "https://www.buyucoin.com/api/v1.2/currency/markets"
    
    var symbol: String = ""
    
    var tradingPair: TradingPair = ("","")
    
    var mainCoin: String = ""
    
    var pairCoin: String = ""
    
    var price: Double = 0
    
    var exchangeName: Exchange = .buyUCoin
    
    var json: JSON = JSON()
    
    private func splitSymbol(symbol: String) -> TradingPair
    {
        let pair = symbol.split(separator: "_")
        return TradingPair(String(describing: pair.first!),String(describing: pair.last!))
    }

}

extension BuyUCoin
{
    init(json: JSON)
    {
        self.json = json
        self.price = json["ask"].doubleValue
        self.buying_price = json["bid"].doubleValue
        self.selling_price = json["ask"].doubleValue
        self.symbol = json["coin"].string!
        self.tradingPair = self.splitSymbol(symbol: symbol)
        self.mainCoin = self.tradingPair.0
        self.pairCoin = self.tradingPair.1
    }
}


