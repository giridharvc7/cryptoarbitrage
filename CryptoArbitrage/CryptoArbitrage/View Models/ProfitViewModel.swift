//
//  ProfitViewModel.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 10/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation


extension ProfitViewModel: Equatable, Comparable
{
    static func < (lhs: ProfitViewModel, rhs: ProfitViewModel) -> Bool
    {
        return lhs.profit_percentage < rhs.profit_percentage
    }
    
    static func == (lhs: ProfitViewModel, rhs: ProfitViewModel) -> Bool
    {
        return lhs.profit_percentage == rhs.profit_percentage
    }
}

struct ProfitViewModel {
    var profit_percentage: Float = 0.0
    var exchange_high: Exchange
    var exchange_high_price: Double
    var exchange_low_price: Double
    var exchange_low: Exchange
    var coin_string: String
    var coin: Coin?
    var high: CryptoViewModel
    var low: CryptoViewModel
    
    
    

    
    init(high: CryptoViewModel, low: CryptoViewModel)
    {
        self.exchange_low = low.exchangeName
        self.exchange_high = high.exchangeName
        self.exchange_high_price = high.price
        self.exchange_low_price = low.price
        self.coin_string = high.mainCoin
        self.high = high
        self.low = low
        self.profit_percentage = CFloat(Calculator.calculateProfitAndPercentage(profitModel: self, quantity: 1000).1)
    }
    
    private func calculateProfit(_ exchange1:CryptoViewModel,_ exchange2: CryptoViewModel) -> Float
    {
//        let difference = Float(exchange2.buyingPrice - exchange1.sellingPrice)
//        return difference/Float(exchange1.sellingPrice)*100
        
        let difference = Float(exchange2.buyingPrice - exchange1.sellingPrice)
        return difference/Float(exchange1.sellingPrice)*100

    }
    
    func actualProfit() -> String
    {
        return Calculator.calculateProfit(profitModel: self, quantity: 1)
    }
    
    func description() -> String
    {
        
        let percentString = String(format:"%.2f",self.profit_percentage)
        return "\(self.coin_string.uppercased()): \n Trade between \(exchange_low.name()) (\(low.sellingPrice)) and \(exchange_high.name())(\(high.buyingPrice)) to get a profit of \(percentString)%"
    }
    
    
    func findCyclingArbitrage()
    {
        
        
    }
}
