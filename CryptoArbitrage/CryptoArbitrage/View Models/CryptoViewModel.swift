//
//  CryptoViewModel.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 10/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation

extension CryptoViewModel: Comparable, Equatable
{
    public static func < (lhs: CryptoViewModel, rhs: CryptoViewModel) -> Bool
    {
        return lhs.sellingPrice < rhs.buyingPrice
    }
    
    public static func == (lhs: CryptoViewModel, rhs: CryptoViewModel) -> Bool
    {
        return lhs.sellingPrice == rhs.buyingPrice
    }
}

public struct CryptoViewModel
{
    var symbol: String
    var tradingPair: TradingPair
    var mainCoin: String
    var pairCoin: String
    var price: Double
    var buyingPrice: Double
    var sellingPrice: Double
    var exchangeName: Exchange
    var tradingFee: TradingFee
    
    
    init(with model: CryptoAsset)
    {
        self.symbol = model.symbol
        self.tradingPair = model.tradingPair
        self.mainCoin = model.mainCoin.lowercased()
        self.pairCoin = model.pairCoin.lowercased()
        self.price = model.price
        self.exchangeName = model.exchangeName
        self.tradingFee = TradingFee(exchange: model.exchangeName)
        self.buyingPrice = model.buying_price
        self.sellingPrice = model.selling_price
    }
    
}
    



