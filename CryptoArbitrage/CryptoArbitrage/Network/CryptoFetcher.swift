//
//  CryptoFetch.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 23/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class CryptoService
{
    static let shared = CryptoService()
    let fetcher: CryptoFetcher
    init()
    {
        fetcher = CryptoFetcher()
        fetcher.start()
        fetcher.startPoll()
    }
}




struct CryptoFetcher
{
    
//    static let shared = CryptoFetcher()
    
    private let disposeBag: DisposeBag = DisposeBag()
    var combinedArray: Variable<[[CryptoViewModel]]> = Variable([[]])
    private var observables: [Observable<[CryptoViewModel]>] = []
    
    private var selected_api_observables: Variable<[Observable<[CryptoViewModel]>]> = Variable([])
    
    let arbitageStatements = Variable<[ProfitViewModel]>([])
    
    var selected_exchanges:[Exchange] = []
    
    let coinDictionary: Variable<CoinDict>  = Variable([:])
    let exchangeDictionary: Variable<CoinDict>  = Variable([:])
    let exchange_names: Variable<[Exchange]> = Variable([])
    let coin_names: Variable<[String]> = Variable([])
//    let exchangeDictionary: Variable

    private var timer: Timer
    init()
    {
        selected_exchanges = [.zebpay,.coinome,.koinex,.coindelta,.bitbns,.unocoin]
        timer = Timer()
    }
    
    func start()
    {
        fetchFromExchanges()
        fetchFees()
        findArbitrages()
    }
    
    func backgroundCall()
    {
        print("Background call")
    }
    
    private func fetchFees()
    {
        let networkManager = NetworkManager()
        networkManager.fetchFees(type: .withdrawalFee)
        networkManager.fetchFees(type: .tradingFee)
        
    }
    
   private var startPolling: Timer {
        return Timer.every(12) {
            self.selected_api_observables.value = self.selected_exchanges.map{getObservable(exchange: $0)}
        }
    }
    
//    var timer: Timer?
     func startPoll()
    {
       let _  = self.startPolling
    }
    
    func stopPolling()
    {
        timer.invalidate()
    }

    
   private func fetchFromExchanges()
    {
        self.selected_api_observables.value = self.selected_exchanges.map{getObservable(exchange: $0)}
        
        selected_api_observables.asObservable().subscribe(onNext:{
            (value) in
            
            Observable.combineLatest(value)
                .subscribe(onNext: { (models) in
                    self.combinedArray.value = models
                }).disposed(by: self.disposeBag)
            
        }).disposed(by: self.disposeBag)
    }
    
    
    
    private func getDataTransformer() -> DataTransformer
    {
        return DataTransformer(allExchanges: self.combinedArray.value, pair: .inr)
    }
    
    private func findArbitrages()
    {
        self.combinedArray.asObservable().skip(1).subscribe(onNext: { models in
//                self.models = models
             let calculator = self.getDataTransformer()
            
                self.arbitageStatements.value = calculator.calculateArbitrage()
                self.coinDictionary.value = calculator.coin_collection
                let exchangeNames = Array(calculator.exchange_collection.keys)
            self.exchange_names.value = exchangeNames.map{
                Exchange(rawValue: $0.lowercased())!
            }
            
            self.coin_names.value = reorderCoins(coinarray: Array(calculator.coin_collection.keys))
            self.exchangeDictionary.value = calculator.exchange_collection
        }).disposed(by: disposeBag)
    }
    
    
    
    
    
    
   
    
    
    
    
   
}
