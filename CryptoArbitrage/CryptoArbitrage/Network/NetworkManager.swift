//
//  ArbitageCalculator.swift
//  CryptoArbitage
//
//  Created by Giridhar on 08/01/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import SwiftyJSON



struct NetworkManager
{
    let disposeBag = DisposeBag()
    
    
    func fetchFees(type: Feenum)
    {
        let url = type.url()
        
        Alamofire.request(url).validate()
            .responseJSON { (jsonData) in
                switch jsonData.result{
                case .success(let jsonValue):
                    //Save in file here
//                    print(JSON(jsonValue))
                    let storageHelper = StorageHelper()
                    storageHelper.write(object: JSON(jsonValue), type: type)
                case .failure(let error):
                    print(error)
                }
        }
        
    }
    
    private func fetchObjects<T:CryptoAsset> (json: JSON, type: Exchange) -> [T]
    {
        switch type {
        case .coindelta, .binance,.zebpay:
            let jsonResult = json
            let objectArray: [T] = jsonResult.map {
                key, value in
                let object = T(json: value)
                return object
            }
            return objectArray
        case .kucoin:
            let jsonResult = json["data"].arrayValue
            let objectArray: [T] = jsonResult.map { value in
                let object = T(json: value)
                return object
            }
            return objectArray
        case .koinex:
            return mapObject(json: json, type: .koinex, key: "stats")
        case .buyUCoin:
            return mapObject(json: json, type: .buyUCoin, key: "data")
        case .coinome,.unocoin:
            return mapObject(json: json, type: type, key: nil)
        case .pocketbits:
            let jsonResult = json["Altcoins"].arrayValue
            let objectArray: [T] = jsonResult.map { value in
                let object = T(json: value)
                return object
            }
            return objectArray
        case .bitbns:
            let jsonResult = json
            let objectArray:[T] = jsonResult.map { dict in
                let object = T(json: dict.1)
                return object
            }
            return objectArray
        }
    }

    
    
    //Works for non selectedarray of exchanges
    func getNewTicker<T: CryptoAsset>(forExchange exchange: T) -> Observable<[CryptoViewModel]>
    {
        return Observable.create{ observer in
            let url = URL(string: exchange.baseURL)
            let request = Alamofire.request(url!)
                .validate()
                .responseJSON{ (jsonData) in
                    switch jsonData.result {
                    case .success(let value):
                        let json = JSON(value)
                        let objectArray:[T] = self.fetchObjects(json: json, type: exchange.exchangeName)
                        let vm_array = objectArray.map{
                                return CryptoViewModel(with: $0)
                        }
                        observer.onNext(vm_array)
                        observer.onCompleted()
                    case .failure(let error):
                        let emptyArray: [CryptoViewModel] = []
                        observer.onNext(emptyArray)
                        observer.onCompleted()
                    }
            }
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    
    func getZebpayTicker() -> Observable<[CryptoViewModel]>
    {
        
        return Observable.create{ observer in
            let zeb_urls = Zebpay().urlList
            let exchange = Exchange.zebpay
            
            let all_api_responses:[Observable<JSON>] = zeb_urls.map{self.getObservableForZebpay(url: $0)}
            
            var zebApi: [JSON] = []
            
//            Observable.combineLatest(all_api_responses).subscribe(onNext: {
//                zebApi = $0
//                let objectsArray:[Zebpay] = self.fetchObjects(json: JSON(zebApi), type: exchange)
//                let vm_array = objectsArray.map{return CryptoViewModel(with: $0)}
//                observer.onNext(vm_array)
//                observer.onCompleted()
//            }).disposed(by: self.disposeBag)

            Observable.combineLatest(all_api_responses).subscribe(onNext: { (json) in
                zebApi = json
                let objectsArray:[Zebpay] = self.fetchObjects(json: JSON(zebApi), type: exchange)
                let vm_array = objectsArray.map{return CryptoViewModel(with: $0)}
                observer.onNext(vm_array)
                observer.onCompleted()
            }, onError: { (error) in
//                observer.onError(error)
                let emptyArray: [CryptoViewModel] = []
                observer.onNext(emptyArray)
                observer.onCompleted()
                
            }).disposed(by: self.disposeBag)
            
            return Disposables.create {
                //FIXME: Not sure what to dispose
            }
        }
        
        
        
        
    }
    
    func getObservableForZebpay(url: String) -> Observable<JSON>
    {
        return Observable.create{ observer in
            let url = URL(string: url)
            let request = Alamofire.request(url!)
                .validate()
                .responseJSON{ (jsonData) in
                    switch jsonData.result {
                    case .success(let value):
                        let json = JSON(value)
                        observer.onNext(json)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        print(error)
                        observer.onCompleted()
                    }
            }
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    
}
