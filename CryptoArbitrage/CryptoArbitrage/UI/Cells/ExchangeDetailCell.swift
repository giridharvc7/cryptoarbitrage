//
//  ExchangeDetailCell.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 31/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import UIKit

class ExchangeDetailCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var coinLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func setupCell(model: ExchangeDetailViewModel)
    {
        self.priceLabel.text = model.priceString
        self.coinLabel.text = model.coin.uppercased()
        self.selectionStyle = .none
    }
    
}
