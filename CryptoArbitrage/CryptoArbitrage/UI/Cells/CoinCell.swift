//
//  CoinCell.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 31/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import UIKit

class CoinCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var coinLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(model: CoinViewModel)
    {
        self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.selectionStyle = .none
        
        self.priceLabel.text = model.averagePriceString
        self.coinLabel.text = model.coinName.uppercased()
    }
    
}
