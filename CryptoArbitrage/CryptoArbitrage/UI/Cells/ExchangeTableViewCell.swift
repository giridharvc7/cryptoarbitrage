//
//  ExchangeTableViewCell.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 31/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import UIKit

class ExchangeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(exchange: Exchange)
    {
        self.nameLabel?.text = exchange.name().capitalizingFirstLetter()
        self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
