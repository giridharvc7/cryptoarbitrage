//
//  TradingFee.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 28/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON
enum TradeAction: String
{
    case buy = "buy"
    case sell = "sell"
}

extension TradingFee
{
    init(json: JSON, exchange: Exchange)
    {
        isMakerFeeModel = json["isMakerFeeModel"].boolValue
        buyer_fee = json["buyer_fee"].doubleValue
        seller_fee = json["seller_fee"].doubleValue
        maker_fee = json["maker_fee"].doubleValue
        taker_fee = json["taker_fee"].doubleValue
        maker_fee_alts = json["maker_fee_alts"].doubleValue
        taker_fee_alts = json["taker_fee_alts"].doubleValue
        self.exchange = exchange
    }
}

struct TradingFee
{
    let isMakerFeeModel: Bool
    let buyer_fee: Double
    let seller_fee: Double
    let maker_fee: Double
    let maker_fee_alts: Double
    let taker_fee_alts: Double
    let taker_fee: Double
    let exchange: Exchange
    
    init(exchange: Exchange)
    {
        let feeJSON = FeeHelper.shared.getTradingFeeJson(exchange: exchange)
        self.init(json: feeJSON, exchange: exchange)
        
    }
    
    func fee(action: TradeAction, coin: String) -> Double
    {
        if self.isMakerFeeModel
        {
            if coin.lowercased() != "btc"
            { return taker_fee_alts }
            else { return taker_fee }
            
        }
        else
        {
            switch action {
            case .buy:
                return buyer_fee
            case .sell:
                return seller_fee
            }
            
        }
        
    }
    
}
