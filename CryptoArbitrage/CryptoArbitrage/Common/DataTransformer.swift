//
//  ArbitageCalculator.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 23/01/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation


struct DataTransformer
{
    var allExchanges: Array<[CryptoViewModel]>
    var pair: Coin
    
   
    
    // Coin as key and exchange names as value
    var flattened_all_exchanges:[CryptoViewModel] {return allExchanges.flatMap{return $0}}
    var flattened_all_exchanges_withPair: [CryptoViewModel]  {return flattened_all_exchanges.filter{
        $0.pairCoin == pair.rawValue
        }}
    // Gets all coins and finds the unique coins across exchanges
    var allCoins:[String]  { return flattened_all_exchanges_withPair.map{$0.mainCoin}}
    var uniqueCoins:[String] { return Array(Set(allCoins))}
    // Decouples and stores coins based on type of coin rather than type of Exchange
    var arrayOfCoins:Array<[CryptoViewModel]> {return uniqueCoins.map{ (coin) -> [CryptoViewModel] in
        let coins = flattened_all_exchanges_withPair.filter{
            value in
            return value.mainCoin == coin
        }
        return coins
        }}
    //Sorts the array of coins which are already filtered based on type of the coin.
    var sortedCoinArrays:Array<[CryptoViewModel]> {return arrayOfCoins.map {$0.sorted()}}
    
    
    var coin_collection: CoinDict {
        var tempDict: CoinDict = [:]
        for sortedCoin in sortedCoinArrays
        {
            let key = sortedCoin.first!.mainCoin
            let value = sortedCoin
            tempDict[key] = value
        }
            return tempDict
    }
    
    var exchange_collection: CoinDict
    {
        var tempDict: CoinDict = [:]
        
//        let exchanges = allExchanges.flatMap{
//            return $0.first
//        }
        
//        let tempDict = exchanges.reduce([], <#T##nextPartialResult: (Result, CryptoViewModel) throws -> Result##(Result, CryptoViewModel) throws -> Result#>)
        
        for exchange in allExchanges
        {
            guard let crypto = exchange.first else { break }
            let key = crypto.exchangeName.name()
            let value = exchange
            tempDict[key] = value
        }

        return tempDict
    }
    
    func calculateArbitrage() -> [ProfitViewModel]
    {
        var profitViewModelList: [ProfitViewModel] = []
        for sortedCoin in sortedCoinArrays
        {
            if (sortedCoin.first?.exchangeName != sortedCoin.last?.exchangeName)
            {
                let profitVM = ProfitViewModel(high: sortedCoin.last!, low: sortedCoin.first!)
                profitViewModelList.append(profitVM)
            }
            profitViewModelList.sort()
            profitViewModelList.reverse()
        }
    
        return profitViewModelList
    }
    
}
