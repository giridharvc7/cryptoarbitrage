//
//  Enums.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 10/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation

enum Exchange: String
{
    case kucoin = "kucoin"
    case binance = "binance"
    case coindelta = "coindelta"
    case koinex = "koinex"
    case bitbns = "bitbns"
    case zebpay = "zebpay"
    case coinome = "coinome"
    case buyUCoin = "buyucoin"
    case pocketbits = "pocketbits"
    case unocoin = "unocoin"
    
    func name() -> String
    {
        return self.rawValue
    }
    
    
//    func tradingFee() -> TradingFee
//    {
//        return FeeHelper().getTradingFeeJson(exchange: self)
//    }
//
    
    func object() -> CryptoAsset
    {
        switch self
        {
            
        case .kucoin:
            return Kucoin()
        case .binance:
            return Binance()
        case .coindelta:
            return CoinDelta()
        case .koinex:
            return Koinex()
        case .bitbns:
            return BitBns()
        case .zebpay:
            return Zebpay()
        case .coinome:
            return Coinome()
        case .buyUCoin:
            return BuyUCoin()
        case .pocketbits:
            return Pocketbits()
        case .unocoin:
            return Unocoin()
        }
    }
    
    func url() -> String
    {
        switch self {
        case .kucoin:
            return "https://api.kucoin.com/v1/open/tick"
        case .binance:
            return "https://api.binance.com/api/v3/ticker/price"
        case .coindelta:
            return  "https://coindelta.com/api/v1/public/getticker/"
        case .koinex:
            return "https://koinex.in/api/ticker"
        case .bitbns:
            return "https://bitbns.com/order/getTickerAll"
        case .zebpay:
            return ""
        case .coinome:
            return "https://www.coinome.com/api/v1/ticker.json"
        case .buyUCoin:
            return "https://www.buyucoin.com/api/v1.2/currency/markets"
        case .pocketbits:
            return "https://pocketbits.in/api/tickerall"
        case .unocoin:
            return "https://www.unocoin.com/trade?exchange"
        }
    }
}
enum Coin: String
{
    case btc = "btc"
    case eth = "eth"
    case ltc = "ltc"
    case neo = "neo"
    case xrp = "xrp"
    case usdt = "usdt"
    case inr = "inr"
    
    public var rawValue: String {
        switch self
        {
        case .btc:
            return "btc"
        case .ltc:
            return "ltc"
        case .neo:
            return "neo"
        case .xrp:
            return "xrp"
        case .eth:
            return "eth"
        case .usdt:
            return "usdt"
        case .inr:
            return "inr"
        }
    }
    
    public var symbol: String {
        switch self
        {
        case .inr:
            return "₹"
        case .usdt:
            return "$"
        default:
            return ""
            
        }
    }
    
}

let accounting_currencies: [Coin] = [.btc, .eth, .usdt, .inr, .ltc]

enum Feenum: String
{
    case withdrawalFee = "withdrawal_fees.json"
    case tradingFee = "trading_fees.json"
    
    func fileName() -> String
    {
        return self.rawValue
    }
    
    func storedFileName() -> String
    {
        switch self
        {
        case .withdrawalFee:
            return "fee_structure"
        case .tradingFee:
            return "trading_fee"
        }
    }
    
    func url() -> String
    {
        switch self
        {
        case .withdrawalFee:
            return "https://giridharvc7.github.io/jsondump/Fees/fees.json"
        case .tradingFee:
            return "https://giridharvc7.github.io/jsondump/Fees/trading_fee.json"
        }
    }
}
