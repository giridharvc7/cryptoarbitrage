//
//  Common.swift
//  CryptoArbitage
//
//  Created by Giridhar on 08/01/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON


protocol CryptoAsset
{
    var symbol: String { get set }
    var tradingPair: TradingPair { get set }
    var mainCoin: String {get set}
    var pairCoin: String {get set}
    var price: Double { get set }
    var buying_price: Double {get set}
    var selling_price: Double {get set}
    var exchangeName: Exchange {get set}
    var baseURL: String{get set}
//    var withdrawal_fee: Double { get set }
//    var trading_fee: Double {get set}

    init()
    init(json: JSON)
    
}

struct CoinStruct
{
    let name: String
    let exchanges: Array<CryptoViewModel>
}








