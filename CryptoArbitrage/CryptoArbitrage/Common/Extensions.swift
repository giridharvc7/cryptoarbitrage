//
//  Extensions.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 28/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation

typealias TradingPair = (String,String)
public typealias CoinDict = [String: Array<CryptoViewModel>]

extension Double {
    func roundedString() -> String
    {
        return String(format:"%.2f",self)
    }
}

extension String {
    
    func contains(_ find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(_ find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func currencyFormat(coin: Coin) -> String {
        return "\(coin.symbol) \(self)"
    }
}


