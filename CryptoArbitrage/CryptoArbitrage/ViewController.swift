
//
//  ViewController.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 08/01/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Whisper

class ViewController: UIViewController {
    let disposeBag: DisposeBag = DisposeBag()
//    lazy var crypto = {
//        return CryptoFetcher()
//    }()
//    @IBOutlet weak var textView: UITextView!
    
    let fetcher = CryptoService.shared.fetcher
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Exchanges"
        navigationController?.navigationBar.prefersLargeTitles = true
    
        navigationController?.navigationBar.layoutIfNeeded()
        let exchanges = Observable.just(fetcher.selected_exchanges)
        
        exchanges.asObservable().bind(to: tableView.rx.items(cellIdentifier: "exchange")){
                (row, exchange: Exchange, cell: ExchangeTableViewCell) in
                cell.setupCell(exchange: exchange)
            }.disposed(by: disposeBag)
        
        
        tableView.register(UINib(nibName: "ExchangeTableViewCell", bundle: nil), forCellReuseIdentifier: "exchange")
        
        tableView.rowHeight = 55
        
        tableView.rx.modelSelected(Exchange.self).subscribe(onNext: { [unowned self] selectedExchange in
            self.pushController(exchange: selectedExchange,animated: true)
            
        }).disposed(by: disposeBag)
        
        pushController(exchange: .koinex, animated: false)
    
    }
    
    func pushController(exchange: Exchange, animated shouldAnimate: Bool)
    {
        let exchangeVC = ExchangeViewController()
        exchangeVC.exchange = exchange
        self.navigationController?.pushViewController(exchangeVC, animated: shouldAnimate)
    }
    
   

}

