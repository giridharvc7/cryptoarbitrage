//
//  ExchangeViewController.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 31/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift


struct ExchangeDetailViewModel
{
    let coin: String
    let priceString: String
    let price: Double
    
    init(model: CryptoViewModel)
    {
        self.coin = model.mainCoin
        self.price = model.price
        var currency = ""
        if model.pairCoin.lowercased() == "inr"
        {
            currency = "₹"
        }
        self.priceString = "\(currency) \(price)"
    }
    
    
}


class ExchangeViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    var exchange: Exchange?
    lazy var tableView: UITableView =  {
        return UITableView(frame: .zero, style: .plain)
    }()
    
    let fetcher = CryptoService.shared.fetcher
    let viewModels: Variable<[ExchangeDetailViewModel]> = Variable([])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = exchange?.name().capitalizingFirstLetter()
        // Do any additional setup after loading the view.
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        
        tableView.register(UINib(nibName: "ExchangeDetailCell", bundle: nil), forCellReuseIdentifier: "exchangeDetailCell")
        
        tableView.rowHeight = 55
        
        let favButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.bookmarks, target: self, action: #selector(favourite))
        
//        let sortButton = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sortTable))
        self.navigationItem.rightBarButtonItems = [favButton]
        
        guard let exchange = exchange else { return }

        fetcher.exchangeDictionary.asObservable().subscribe(onNext:{ value in
            let models = ViewModelFactory().buildModels(dict: value, exchange: exchange)
            self.viewModels.value = models
            
        }).disposed(by: disposeBag)
        
        self.viewModels.asObservable().bind(to: tableView.rx.items(cellIdentifier: "exchangeDetailCell")){
            (row, model: ExchangeDetailViewModel, cell: ExchangeDetailCell) in
            
            cell.setupCell(model: model)
            
            }.disposed(by: disposeBag)
        
//        let viewModels: Variable<ExchangeDetailViewModel> = Variable(ExchangeDetailViewModel)
        
        
//        fetcher.coinDictionary.asObservable().bind(to: tableView.rx.items(cellIdentifier: "exchangeDetailCell")){
//            (row, coin: CoinDict, cell: ExchangeDetailCell) in
//
//            }.disposed(by: disposeBag)
        // Do any additional setup after loading the view.
    }
    
    
    @objc func favourite()
    {
        
    }
    
    @objc func sortTable()
    {
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
