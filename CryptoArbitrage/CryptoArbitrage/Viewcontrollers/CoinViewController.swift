//
//  CoinViewController.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 31/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import UIKit
import RxSwift

struct CoinViewModel
{
    let coinName: String
    private let exchangeData: [CryptoViewModel]
    let averagePrice: Double
    var averagePriceString: String = ""
    let lowestExchange: String = ""
    let highestExchange: String = ""
    
    init(coinName: String, exchangeData: [CryptoViewModel])
    {
        self.coinName = coinName
        self.exchangeData = exchangeData
        
        
        let filteredCoins = exchangeData.filter{return $0.pairCoin.lowercased() == "inr"}
        self.averagePrice = filteredCoins.map{ return $0.price}.reduce(0.0) { Double($0) + Double($1)/Double(exchangeData.count)}
        
        self.averagePriceString = "\(self.averagePrice.roundedString())".currencyFormat(coin: .inr)
        
        
        
        
    }
    
}

class CoinViewController: UIViewController
{
    
    @IBOutlet weak var tableView: UITableView!
    let disposeBag: DisposeBag = DisposeBag()
    let crypto = CryptoService.shared.fetcher
    
    let viewModels: Variable<[CoinViewModel]> = Variable([])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cryptocurrencies"

        navigationController?.navigationBar.prefersLargeTitles = true
        
        navigationController?.navigationBar.layoutIfNeeded()
        

        
        tableView.register(UINib(nibName: "CoinCell", bundle: nil), forCellReuseIdentifier: "coin")
        tableView.rowHeight = 55
        
        crypto.coinDictionary.asObservable().subscribe(onNext:{ value in
            
//            let models = ViewModelFactory().buildModels(dict: <#T##CoinDict#>, exchange: <#T##Exchange#>)
            
            let models = ViewModelFactory().buildModels(dict: value)
            self.viewModels.value = models
        }).disposed(by: disposeBag)
        
        
        
        viewModels.asObservable().bind(to: tableView.rx.items(cellIdentifier: "coin")){
            (row, model: CoinViewModel, cell: CoinCell) in
            
            cell.setupCell(model: model)
            
            }.disposed(by: disposeBag)
        // Do any additional setup after loading the view.
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
//        crypto.coin_names.asObservable().bind(to: tableView.rx.items(cellIdentifier: "coin")){
//            (row, coin: String, cell: UITableViewCell) in
////            cell.textLabel?.text = coin
//            }.disposed(by: disposeBag)

        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
