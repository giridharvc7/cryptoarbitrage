//
//  GeneralHelper.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 31/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation

struct ViewModelFactory
{
    func buildModels(dict: CoinDict, exchange: Exchange) -> [ExchangeDetailViewModel]
    {
        let coins = dict[exchange.name().lowercased()]
        guard let coinArray = coins else { return [] }
        
        let viewModels = coinArray.filter { (model) -> Bool in
            return model.pairCoin.lowercased() == "inr"
            }.map{
                return ExchangeDetailViewModel(model: $0)
        }
    
        
        let prefferedCoins:[Coin] = [.btc,.eth,.xrp,.neo,.ltc]
        let prefferedCoinsString = prefferedCoins.map {$0.rawValue.lowercased()}
        
        var filteredCoins = viewModels.filter { (model) -> Bool in
            return prefferedCoinsString.contains(model.coin.lowercased())
        }
        filteredCoins = filteredCoins.sorted { (model1, model2) -> Bool in
            return model1.coin <  model2.coin
        }
        
        let filteredCoins2 = viewModels.filter { (model) -> Bool in
            return !prefferedCoinsString.contains(model.coin.lowercased())
        }
        
        filteredCoins.append(contentsOf: filteredCoins2)
        
        return filteredCoins
        
    }
    
    
    func buildModels(dict: CoinDict) -> [CoinViewModel]
    {
        let coins = reorderCoins(coinarray: Array(dict.keys))
        
        let models =  coins.map{ (coin) -> CoinViewModel in
            let exchangeArray = dict[coin]!
            return CoinViewModel(coinName: coin, exchangeData: exchangeArray)
            
        }
        return models
    }

    
    

}

func reorderCoins(coinarray: [String]) -> [String]
{
    let coins = coinarray.map{
        return $0.lowercased()
    }
    let prefferedCoins:[Coin] = [.btc,.eth,.xrp,.neo,.ltc]
    let prefferedCoinString:[String] = prefferedCoins.map{
        return $0.rawValue
    }
    let setPreffered = Set(prefferedCoinString)
    let fullSet = Set(coins)
    
    let intersecting = fullSet.subtracting(setPreffered)
    var intersectArray = Array(intersecting).sorted()
    intersectArray.insert(contentsOf: prefferedCoinString, at: 0)
    
    return intersectArray
    
}



