//
//  FeeHelper.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 28/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

class FeeHelper {
    static let shared = FeeHelper()
    let withdrawal_fee: JSON
    let trading_fee: JSON
    let storageHelper = StorageHelper()
    init()
    {
        withdrawal_fee = storageHelper.readFees(type: .withdrawalFee)
        trading_fee = storageHelper.readFees(type: .tradingFee)
    }
    
    //    func getTradingFee(exchange: Exchange) -> TradingFee
    //    {
    //        let fromTradingFeeJson = trading_fee[exchange.name().lowercased()]
    //        return TradingFee(exchange: <#T##Exchange#>)
    //    }
    
    func getTradingFeeJson(exchange: Exchange) -> JSON
    {
        return trading_fee[exchange.name().lowercased()]
    }
    
    //    func getTradingFee(fromExchange: Exchange, toExchange: Exchange) -> (TradingFee, TradingFee)
    //    {
    //        let fromTradingFeeJson = trading_fee[fromExchange.name().lowercased()]
    //        let toTradingFeeJson = trading_fee[toExchange.name().lowercased()]
    //
    //        return (TradingFee(json: fromTradingFeeJson), TradingFee(json: toTradingFeeJson))
    //    }
    
    func getWithdrawalFee(coin: String, exchange: Exchange) -> Double
    {
        let exchange_info = withdrawal_fee[coin.uppercased()]
        var fee: Double = 0
        let dict = exchange_info["common"]
        fee = dict["flat_fee"].doubleValue
        
        let exchange_dict = exchange_info[exchange.name()]
        if exchange_dict != JSON.null
        {
            fee = exchange_dict["flat_fee"].doubleValue
        }
        
        return fee
    }
}


