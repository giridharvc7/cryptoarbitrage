//
//  StorageHelper.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 24/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import SwiftyJSON

struct StorageHelper
{
    let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    func write(object: JSON, type: Feenum)
    {
        let writePath = documents.appending("/\(type.fileName())")
        let rawData = try! object.rawData()
        try! rawData.write(to: URL(fileURLWithPath: writePath))
        
//        let _ = FeeHelper()
    }
    
    func readFees(type: Feenum) -> JSON
    {
        let readPath = documents.appending(type.fileName())
        do
        {
            let data = try Data(contentsOf: URL(fileURLWithPath: readPath))
            return JSON(data)
        }
        catch
        {
            return readDefaultFees(type: type)
        }
    }
    
    private func readDefaultFees(type: Feenum) -> JSON
    {
        let readPath =  Bundle.main.url(forResource: type.storedFileName(), withExtension: "json")
        let data = try! Data(contentsOf: readPath!)
        return JSON(data)
    }
    
}
