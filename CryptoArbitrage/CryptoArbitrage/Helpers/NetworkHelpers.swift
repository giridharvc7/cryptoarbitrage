//
//  Helpers.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 10/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

func getObservable(exchange: Exchange) -> Observable<[CryptoViewModel]>
{
    let networkManager = NetworkManager()
    switch exchange
    {
        
    case .kucoin:
        return networkManager.getNewTicker(forExchange: Kucoin())
    case .binance:
        return networkManager.getNewTicker(forExchange: Binance())
    case .coindelta:
        return networkManager.getNewTicker(forExchange: CoinDelta())
    case .koinex:
        return networkManager.getNewTicker(forExchange: Koinex())
    case .bitbns:
        return networkManager.getNewTicker(forExchange: BitBns())
    case .zebpay:
        return networkManager.getZebpayTicker()
    case .coinome:
        return networkManager.getNewTicker(forExchange: Coinome())
    case .buyUCoin:
        return networkManager.getNewTicker(forExchange: BuyUCoin())
    case .pocketbits:
        return networkManager.getNewTicker(forExchange: Pocketbits())
    case .unocoin:
        return networkManager.getNewTicker(forExchange: Unocoin())
    }
}

func mapObject<T:CryptoAsset> (json: JSON, type: Exchange,key: String?) -> [T]
{
    var subJson = json
    
    if key != nil
    {
        subJson = json[key!]
    }
    
    if (type == .koinex || type == .buyUCoin || type == .coinome || type == .unocoin)
    {
        let objectArray: [T] = subJson.map{ dict in
            var newDict = dict.1
            newDict["coin"].string = dict.0
            let object = T(json: newDict)
            return object
        }
        return objectArray
    }
    return []
}

 

