//
//  PLCalculator.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 28/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import Foundation

struct Calculator
{
   static func calculateProfit(from: CryptoViewModel, to: CryptoViewModel, buyingValue: Double) -> (Double, Double)
    {
        let fromTradeFee = from.tradingFee.fee(action: .buy, coin: from.mainCoin)
        let toTradeFee = to.tradingFee.fee(action: .sell, coin: to.mainCoin)
        
        let fromPrice = from.sellingPrice
        let toPrice = to.buyingPrice
        let withdrawalFee = FeeHelper.shared.getWithdrawalFee(coin: from.mainCoin, exchange: from.exchangeName)
        //Bought on exchange 1
        let feeAdjustedBuyingValue = (buyingValue) - (buyingValue*(fromTradeFee/100))
        let quantity = feeAdjustedBuyingValue/fromPrice
        let effective_quantity = quantity - withdrawalFee
        
        // Sent to exchange 2
        
        //Selling on Exchange 2
        
        let sellingCost = (effective_quantity * toPrice)
        let sellingFee = sellingCost * (toTradeFee/100)
        let effectiveSellingValue = sellingCost - sellingFee
        let profit = effectiveSellingValue - buyingValue
        let percentage = (profit/buyingValue) * 100
        return (profit,percentage)
    }
    
    static func calculateProfitAndPercentage(profitModel: ProfitViewModel, quantity: Double) -> (Double,Double)
    {
        
        let fromAsset = profitModel.low
        let toAsset  = profitModel.high
        return Calculator.calculateProfit(from: fromAsset, to: toAsset, buyingValue: quantity)
        
//        return "Profit: \(profit.roundedString()) and actual percentage of Profit: \(percentage.roundedString())"
        
        
        //        transformer.calculateProfit(coin: <#T##String#>, from: <#T##Exchange#>, to: <#T##Exchange#>, quantity: <#T##Double#>)
    }
    
    
    static func calculateProfit(profitModel: ProfitViewModel, quantity: Double) -> String
    {
        
        let fromAsset = profitModel.low
        let toAsset  = profitModel.high
        let (profit, percentage) = Calculator.calculateProfit(from: fromAsset, to: toAsset, buyingValue: quantity)
        
        return "Profit: \(profit.roundedString()) and actual percentage of Profit: \(percentage.roundedString())"
        
        
        //        transformer.calculateProfit(coin: <#T##String#>, from: <#T##Exchange#>, to: <#T##Exchange#>, quantity: <#T##Double#>)
    }
    
}



