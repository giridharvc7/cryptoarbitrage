//
//  TestViewController.swift
//  CryptoArbitrage
//
//  Created by Giridhar on 31/03/18.
//  Copyright © 2018 Giri. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ArbitrageViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag: DisposeBag = DisposeBag()
//    lazy var crypto = {
//        return CryptoFetcher()
//    }()
    
    let crypto = CryptoService.shared.fetcher
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        self.title = "Arbitrage"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        navigationController?.navigationBar.layoutIfNeeded()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Sample")

        fetchResults()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchResults()
    
    {
        crypto.arbitageStatements.asObservable().bind(to: tableView.rx.items(cellIdentifier: "Sample")){
                (row, profit: ProfitViewModel, cell: UITableViewCell) in
    
                cell.textLabel?.numberOfLines = 3
                cell.textLabel?.text = profit.description()
    
    
                }.disposed(by: disposeBag)
        
    
                
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
